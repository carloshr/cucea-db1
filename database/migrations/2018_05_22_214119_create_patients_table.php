<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 35);
            $table->string('last_name', 35);
            $table->string('file_number', 15)->nullable();
            $table->string('room', 8)->nullable();
            $table->string('hall', 8)->nullable();
            $table->string('insurance', 30)->nullable();
            $table->string('marital_status', 10)->nullable();
            $table->date('dob');
            $table->string('gender', 1);
            $table->string('scholarship', 40)->nullable();
            $table->string('religion', 30)->nullable();
            $table->string('birthplace', 30)->nullable();
            $table->string('ethnic_group', 30)->nullable();
            $table->string('occupation', 30)->nullable();
            $table->string('sports', 200)->nullable();
            $table->string('blood_type', 3)->nullable();
            $table->string('responsable', 6)->nullable();
            $table->string('responsable_name', 30)->nullable();
            $table->string('referred', 30)->nullable();

            $table->string('email', 60)->nullable();
            $table->string('phone1', 20)->nullable();
            $table->string('phone2', 20)->nullable();
            $table->string('emergency_phone', 20)->nullable();
            $table->string('housing_type', 20)->nullable();
            $table->string('address_line_1', 200)->nullable();
            $table->string('address_line_2', 200)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('country', 8)->nullable();
            $table->string('state', 8)->nullable();
            $table->string('city', 8)->nullable();

            $table->string('image', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
