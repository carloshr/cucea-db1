<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->integer('hemoglobin')->unsigned()->nullable();
            $table->integer('glucoce')->unsigned()->nullable();
            $table->integer('glucoce_after_eating')->unsigned()->nullable();
            $table->decimal('urine_ketones', 4, 2)->nullable();
            $table->decimal('bood_ketones', 4, 2)->nullable();
            $table->integer('cholesterol')->unsigned()->nullable();
            $table->integer('cholesterol_ldl')->unsigned()->nullable();
            $table->integer('cholesterol_hdl')->unsigned()->nullable();
            $table->string('blood_pressure', 10)->nullable();
            $table->integer('triglycerides')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labs');
    }
}
