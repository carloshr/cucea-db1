<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePLMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_l_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plm_code', 10);
            $table->string('name', 30);
            $table->string('substance', 30);
            $table->string('pharmaceutical_form', 30);
            $table->string('presentation', 30);
            $table->string('lab', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_l_ms');
    }
}
