<?php

use Illuminate\Database\Seeder;

class CIE10TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
        	DB::table('c_i_e10s')->insert([
        		'cie10_code' => str_random(rand(1, 2)) . rand(1, 100),
        		'cie10_diagnosis' => str_random(rand(8, 18)),
        		'cie10_description' => str_random(rand(100, 200)),
        	]);
        }
    }
}
