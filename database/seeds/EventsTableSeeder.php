<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
        	$date = Carbon\Carbon::now()->addDays(rand(1, 20));
        	DB::table('events')->insert([
        		'patient_id' => rand(1, 50),
        		'title' => str_random(rand(8, 12)),
        		'start' => $date,
        		'end' => $date->addMinutes((bool) rand(0, 1) ? 30 : 45),
        		'hex_color' => '252525',
        		'arrived_at' => (bool) rand(0, 1) ? $date : null
        	]);
        }
    }
}
