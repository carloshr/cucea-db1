<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PatientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
        for ($i = 0; $i < 50; $i++) {
        	$firstName = $faker->firstName;
        	$lastName = $faker->lastName;
        	DB::table('patients')->insert([
        		'file_number' => $i + 1,
        		'first_name' => $firstName,
        		'last_name' => $lastName,
        		'dob' => Carbon\Carbon::now(),
        		'gender' => (bool) rand(0, 1) ? 'm' : 'f',
        		'email' => strtolower($firstName).'.'.strtolower($lastName).'@cucea.udg.mx',
        		'phone1' => $faker->e164PhoneNumber,
        		'phone2' => $faker->e164PhoneNumber
        	]);
        }
    }
}
